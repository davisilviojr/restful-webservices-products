# RESTFUL WEBSERVICES PRODUCTS #

Restful Jax-RS service to perform CRUD operations on a Product resource using Image as a sub-resource of Product.

### Run Application ###

* mvn spring-boot:run

### Run Tests ###

* mvn test

### Examples of each call ###

=> CREATE PRODUCT

POST http://localhost:8080/wsproducts/products/

{
	"name": "product 1",
	"description": "product 1 desc"
}

=> UPDATE PRODUCT

POST http://localhost:8080/wsproducts/products/898

{
	"name": "product 1",
	"description": "product 1 desc"
}

=> LIST ALL PRODUCT WITH RELATIONSHIPS

GET http://localhost:8080/wsproducts/products/all

=> LIST ALL PRODUCT WITHOUT RELATIONSHIPS

GET http://localhost:8080/wsproducts/products/

=> GET PRODUCT BY ID WITHOUT RELATIONSHIPS

GET http://localhost:8080/wsproducts/products/897

=> GET PRODUCT BY ID WITH RELATIONSHIPS

GET http://localhost:8080/wsproducts/products/897/all

=> DESTROY PRODUCT

DELETE http://localhost:8080/wsproducts/products/898

=> CREATE SUBPRODUCT

POST http://localhost:8080/wsproducts/products/

{
	"name": "subproduct 2",
	"description": "subproduct 2 desc",
	"parentProduct": { "id": "898" }
}

=> LIST ALL PRODUCT IMAGES

GET http://localhost:8080/wsproducts/products/898/images

=> CREATE PRODUCT IMAGE

POST http://localhost:8080/wsproducts/products/898/images

{
	"type": "jpeg"
}

=> UPDATE PRODUCT IMAGE

POST http://localhost:8080/wsproducts/products/898/images/437

{
	"type": "jpeg"
}

=> DESTROY PRODUCT IMAGE

DELETE http://localhost:8080/wsproducts/products/899/images/387

