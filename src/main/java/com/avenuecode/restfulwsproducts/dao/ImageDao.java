/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.dao;


import com.avenuecode.restfulwsproducts.model.Image;

import java.util.List;

public interface ImageDao {
    public void create (Image image);
    public void update (Image image);
    public Image getImageById (long productId, long id);
    public List<Image> getAllImages (long productId);
    public void delete (Image image);
}
