/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.dao.impl;

import com.avenuecode.restfulwsproducts.dao.ImageDao;
import com.avenuecode.restfulwsproducts.model.Image;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ImageDaoImpl implements ImageDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(Image image) {
        entityManager.persist(image);
    }

    public void update(Image image) {
        entityManager.merge(image);
    }

    public Image getImageById(long productId, long id) {
        String query = "SELECT i FROM Image i where i.product.id =" +
                productId + "and i.id = " + id;
        Image image = null;
        List<Image> list = entityManager.createQuery(query).getResultList();
        if (!list.isEmpty()) {
            image = list.get(0);
        }
        return image;
    }

    public List<Image> getAllImages(long productId) {
        return entityManager.createQuery("SELECT i FROM Image i where i.product.id =" +
                productId).getResultList();
    }

    public void delete(Image image) {
        entityManager.remove(entityManager.contains(image) ? image : entityManager.merge(image));
    }
}
