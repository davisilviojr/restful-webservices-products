/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.dao.impl;

import com.avenuecode.restfulwsproducts.dao.ProductDao;
import com.avenuecode.restfulwsproducts.model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create (Product product) {
        entityManager.persist(product);
    }

    public void update (Product product) {
        entityManager.merge(product);
    }

     public Product getProductById (long id) {
        String query = "SELECT new Product(p.id, p.name, p.description) " +
                "FROM Product p where p.id =" + id;
        Product product = null;
        List<Product> list = entityManager.createQuery(query).getResultList();
        if (!list.isEmpty()) {
            product = list.get(0);
        }
        return product;
    }

    public Product getProductWithRelationships(long id) {
        return entityManager.find(Product.class, id);
    }

    public List<Product> getAllProducts() {
        return entityManager.createQuery("SELECT new Product(p.id, p.name, p.description) FROM Product p").getResultList();
    }

    public List<Product> getAllSubProducts(long id) {
        return entityManager.createQuery("SELECT p FROM Product p where p.parentProduct.id = " + id).getResultList();
    }

    public List<Product> getAllProductsWithRelationships() {
        return entityManager.createQuery("SELECT p FROM Product p where p.parentProduct = null").getResultList();
    }

    public void delete (Product product) {
        entityManager.remove(entityManager.contains(product) ? product : entityManager.merge(product));
    }
}
