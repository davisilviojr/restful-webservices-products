/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.dao;

import com.avenuecode.restfulwsproducts.model.Product;

import java.util.List;

public interface ProductDao {
    public void create (Product product);
    public void update (Product product);
    public Product getProductById (long id);
    public Product getProductWithRelationships (long id);
    public List<Product> getAllSubProducts (long id);
    public List<Product> getAllProducts ();
    public List<Product> getAllProductsWithRelationships();
    public void delete (Product product);
}
