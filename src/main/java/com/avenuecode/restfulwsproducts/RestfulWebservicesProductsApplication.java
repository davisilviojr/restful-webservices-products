/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulWebservicesProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulWebservicesProductsApplication.class, args);
	}
}
