/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.service.impl;

import com.avenuecode.restfulwsproducts.dao.ImageDao;
import com.avenuecode.restfulwsproducts.model.Image;
import com.avenuecode.restfulwsproducts.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Component
public class ImageServiceImpl implements ImageService {
    @Autowired
    private ImageDao imageDao;

    public void create (Image image) {
        imageDao.create(image);
    }

    public Image getImageById (long productId, long id) {
        return imageDao.getImageById(productId, id);
    }

    public void delete (Image image) {
        imageDao.delete(image);
    }

    public List<Image> getAllImages (long productId) {
        return imageDao.getAllImages(productId);
    }

    public void update (Image image) {
        imageDao.update(image);
    }
}
