/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.service.impl;

import com.avenuecode.restfulwsproducts.dao.ProductDao;
import com.avenuecode.restfulwsproducts.model.Product;
import com.avenuecode.restfulwsproducts.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Component
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;

    public void create (Product product) {
        productDao.create(product);
    }

    public Product getProductById (long id) {
        return productDao.getProductById(id);
    }

    @Override
    public List<Product> getAllSubProducts(long id) {
        return productDao.getAllSubProducts(id);
    }

    public Product getProductWithRelationships(long id) {
        return productDao.getProductWithRelationships(id);
    }

    public void delete (Product product) {
        productDao.delete(product);
    }

    public List<Product> getAllProducts () {
        return productDao.getAllProducts();
    }

    public List<Product> getAllProductsWithRelationships () {
        return productDao.getAllProductsWithRelationships();
    }

    public void update (Product product) {
        productDao.update(product);
    }
}
