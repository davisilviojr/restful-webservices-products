/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.service;

import com.avenuecode.restfulwsproducts.model.Image;

import java.util.List;

public interface ImageService {
    public void create (Image image);
    public Image getImageById (long productId, long id);
    public void delete (Image image);
    public List<Image> getAllImages (long productId);
    public void update (Image image);
}
