/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.service;

import com.avenuecode.restfulwsproducts.model.Product;

import java.util.List;

public interface ProductService {
    public void create (Product product);
    public Product getProductById (long id);
    public Product getProductWithRelationships (long id);
    public List<Product> getAllSubProducts (long id);
    public void delete (Product product);
    public List<Product> getAllProducts ();
    public List<Product> getAllProductsWithRelationships ();
    public void update (Product product);
}
