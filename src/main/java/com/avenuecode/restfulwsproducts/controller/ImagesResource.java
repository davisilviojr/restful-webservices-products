/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.controller;

import com.avenuecode.restfulwsproducts.exceptions.ErrorMessage;
import com.avenuecode.restfulwsproducts.model.Image;
import com.avenuecode.restfulwsproducts.model.Product;
import com.avenuecode.restfulwsproducts.service.ImageService;
import com.avenuecode.restfulwsproducts.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Produces("application/json")
@Consumes("application/json")
public class ImagesResource {
    @Autowired
    private ImageService service;

    @Autowired
    private ProductService productService;

    private long productId;

    public void setProductId(long productId) {
        this.productId = productId;
    }

    // GET: /products/123/images
    @GET
    public Response getAllImagesByProductId () {
        List<Image> images = service.getAllImages(this.productId);
        if (images == null || images.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "No image found for the given product")).build();
        } else {
            return Response.ok(images, MediaType.APPLICATION_JSON).build();
        }
    }

    // POST: /products/123/images
    @POST
    public Response createImage (Image image) {
        Product product = productService.getProductById(this.productId);
        if (product == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Product not found")).build();
        } else if(image == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorMessage(400, "Bad request")).build();
        } else {
            image.setProduct(product);
            service.create(image);
            return Response.status(Response.Status.CREATED).build();
        }
    }

    // POST: /products/123/images/123
    @POST
    @Path("/{id}")
    public Response updateImage (@PathParam(value = "id") long id, Image image) {
        Image imageUpdated = service.getImageById(this.productId, id);
        if (imageUpdated == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        } else if (image == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorMessage(400, "Bad request")).build();
        } else {
            imageUpdated.setType(image.getType());
            service.update(imageUpdated);
            return Response.status(Response.Status.CREATED).build();
        }
    }

    // DELETE: /products/123/images/123
    @DELETE
    @Path("/{id}")
    public Response deleteImage (@PathParam(value = "id") long id) {
        Image image = service.getImageById(this.productId, id);
        if (image != null) {
            service.delete(image);

            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        }
    }
}
