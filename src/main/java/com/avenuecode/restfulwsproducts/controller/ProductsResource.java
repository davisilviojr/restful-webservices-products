/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.controller;

import com.avenuecode.restfulwsproducts.exceptions.ErrorMessage;
import com.avenuecode.restfulwsproducts.model.Product;
import com.avenuecode.restfulwsproducts.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/products")
@Produces("application/json")
@Consumes("application/json")
public class ProductsResource {
    @Autowired
    private ProductService service;

    @Autowired
    private ImagesResource imageResource;

    // GET: /products/123
    @GET
    @Path("/{id}")
    public Response getProduct (@PathParam(value = "id") long id) {
        Product product = service.getProductById(id);
        if (product == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        } else {
            return Response.ok(product, MediaType.APPLICATION_JSON).build();
        }
    }

    // GET: /products/123/all
    @GET
    @Path("/{id}/all")
    public Response getProductWithRelationships (@PathParam(value = "id") long id) {
        Product product = service.getProductWithRelationships(id);
        if (product == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        } else {
            return Response.ok(product, MediaType.APPLICATION_JSON).build();
        }
    }

    // GET: /products/123/subproducts
    @GET
    @Path("/{id}/subproducts")
    public Response getAllSubproducts (@PathParam(value = "id") long id) {
        List<Product> products = service.getAllSubProducts(id);
        if (products == null || products.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entities not found")).build();
        } else {
            return Response.ok(products, MediaType.APPLICATION_JSON).build();
        }
    }

    // GET: /products
    @GET
    public Response getAllProducts () {
        List<Product> products = service.getAllProducts();
        if (products == null || products.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entities not found")).build();
        } else {
            return Response.ok(products, MediaType.APPLICATION_JSON).build();
        }
    }

    // GET: /products/all
    @GET
    @Path("/all")
    public Response getAllProductsWhitRelationships () {
        List<Product> products = service.getAllProductsWithRelationships();
        if (products == null || products.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entities not found")).build();
        } else {
            return Response.ok(products, MediaType.APPLICATION_JSON).build();
        }
    }

    // POST: /products
    @POST
    public Response createProduct (Product product) {
        if (product == null){
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorMessage(400, "Bad request")).build();
        } else {
            service.create(product);
            return Response.status(Response.Status.CREATED).build();
        }
    }

    // POST: /products/123
    @POST
    @Path("/{id}")
    public Response updateProduct (@PathParam(value = "id") long id, Product product) {
        Product productUpdated = service.getProductWithRelationships(id);
        if (productUpdated != null && product != null) {
            productUpdated.setName(product.getName());
            productUpdated.setDescription(product.getDescription());

            service.update(productUpdated);

            return Response.status(Response.Status.CREATED).build();
        } else if (product == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorMessage(400, "Bad request")).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        }
    }

    // DELETE: /products/123
    @DELETE
    @Path("/{id}")
    public Response deleteProduct (@PathParam(value = "id") long id) {
        Product product = service.getProductWithRelationships(id);
        if (product != null) {
            service.delete(product);

            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessage(404, "Entity not found")).build();
        }
    }

    @Path("{id}/images")
    public ImagesResource getImagesResourceByProductId(@PathParam("id") long id) {
        imageResource.setProductId(id);
        return imageResource;
    }
}
