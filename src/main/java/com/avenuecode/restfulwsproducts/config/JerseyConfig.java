/*******************************************************************************
 * Author   : Davi Junior
 * Data     : 08/10/2017
 *******************************************************************************/

package com.avenuecode.restfulwsproducts.config;

import com.avenuecode.restfulwsproducts.controller.ImagesResource;
import com.avenuecode.restfulwsproducts.controller.ProductsResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/wsproducts")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerEndpoints();
    }

    private void registerEndpoints(){
        register(ProductsResource.class);
        register(ImagesResource.class);
    }
}
