package com.avenuecode.restfulwsproducts.controller;

import com.avenuecode.restfulwsproducts.model.Product;
import com.avenuecode.restfulwsproducts.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductsResourceTest {
    private static final String PRODUCTS = "/wsproducts/products";

    private MockMvc mockMvc;

    @InjectMocks
    private ProductsResource productsResource;

    @Mock
    private ProductService service;

    @Mock
    private ImagesResource imgResource;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders
                .standaloneSetup(productsResource)
                .build();

        ReflectionTestUtils.setField(productsResource, "service", service);
        ReflectionTestUtils.setField(productsResource, "imageResource", imgResource);
    }

    @Test
    public void getProducts() throws Exception {
        given(service.getAllProducts()).willReturn(new ArrayList<Product>());

//        mockMvc.perform(post(PRODUCTS).contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON).content(new Gson().toJson(mockProduct()))).andExpect(status().isCreated());

    }

    private Product mockProduct() {
        return new Product(1L, "test", "test");
    }
}
